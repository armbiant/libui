/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtGui>
#include "desqui/FlexiLayout.hpp"

DesQUI::FlexiLayout::FlexiLayout( QWidget *parent ) : QLayout( parent ) {
    mLayoutMode = DesQUI::FlexiLayout::Centered;

    m_hSpace = smartSpacing( QStyle::PM_LayoutHorizontalSpacing );
    m_vSpace = smartSpacing( QStyle::PM_LayoutVerticalSpacing );

    int lmargin = smartSpacing( QStyle::PM_LayoutLeftMargin );
    int tmargin = smartSpacing( QStyle::PM_LayoutTopMargin );
    int rmargin = smartSpacing( QStyle::PM_LayoutRightMargin );
    int bmargin = smartSpacing( QStyle::PM_LayoutBottomMargin );

    QLayout::setContentsMargins( lmargin, tmargin, rmargin, bmargin );
}


DesQUI::FlexiLayout::FlexiLayout() : QLayout() {
    mLayoutMode = DesQUI::FlexiLayout::Centered;

    m_hSpace = smartSpacing( QStyle::PM_LayoutHorizontalSpacing );
    m_vSpace = smartSpacing( QStyle::PM_LayoutVerticalSpacing );

    int lmargin = smartSpacing( QStyle::PM_LayoutLeftMargin );
    int tmargin = smartSpacing( QStyle::PM_LayoutTopMargin );
    int rmargin = smartSpacing( QStyle::PM_LayoutRightMargin );
    int bmargin = smartSpacing( QStyle::PM_LayoutBottomMargin );

    QLayout::setContentsMargins( lmargin, tmargin, rmargin, bmargin );
}


DesQUI::FlexiLayout::~FlexiLayout() {
    QLayoutItem *item;

    while ( (item = takeAt( 0 ) ) ) {
        delete item;
    }
}


void DesQUI::FlexiLayout::addItem( QLayoutItem *item ) {
    itemList.append( item );
}


void DesQUI::FlexiLayout::clear() {
    QLayoutItem *item;

    Q_FOREACH ( item, itemList ) {
        delete item;
    }
}


int DesQUI::FlexiLayout::horizontalSpacing() const {
    if ( m_hSpace >= 0 ) {
        return m_hSpace;
    }

    else {
        return smartSpacing( QStyle::PM_LayoutHorizontalSpacing );
    }
}


void DesQUI::FlexiLayout::setHorizontalSpacing( int h_space ) {
    if ( h_space >= 0 ) {
        m_hSpace = h_space;
    }

    else {
        m_hSpace = smartSpacing( QStyle::PM_LayoutHorizontalSpacing );
    }

    doLayout( mGeometry, false );
}


int DesQUI::FlexiLayout::verticalSpacing() const {
    if ( m_vSpace >= 0 ) {
        return m_vSpace;
    }

    else {
        return smartSpacing( QStyle::PM_LayoutVerticalSpacing );
    }
}


void DesQUI::FlexiLayout::setVerticalSpacing( int v_space ) {
    if ( v_space >= 0 ) {
        m_vSpace = v_space;
    }

    else {
        m_vSpace = smartSpacing( QStyle::PM_LayoutVerticalSpacing );
    }

    doLayout( mGeometry, false );
}


DesQUI::FlexiLayout::LayoutMode DesQUI::FlexiLayout::layoutMode() const {
    return (DesQUI::FlexiLayout::LayoutMode)mLayoutMode;
}


void DesQUI::FlexiLayout::setLayoutMode( DesQUI::FlexiLayout::LayoutMode mode ) {
    mLayoutMode = mode;
    doLayout( mGeometry, false );
}


QMargins DesQUI::FlexiLayout::contentsMargins() const {
    int left, top, right, bottom;

    getContentsMargins( &left, &top, &right, &bottom );

    return QMargins( left, top, right, bottom );
}


void DesQUI::FlexiLayout::setContentsMargins( QMargins margins ) {
    QLayout::setContentsMargins( margins );
    doLayout( mGeometry, false );
}


int DesQUI::FlexiLayout::count() const {
    return itemList.size();
}


QLayoutItem *DesQUI::FlexiLayout::itemAt( int index ) const {
    return itemList.value( index );
}


QLayoutItem *DesQUI::FlexiLayout::takeAt( int index ) {
    if ( (index >= 0) && (index < itemList.size() ) ) {
        return itemList.takeAt( index );
    }

    else {
        return 0;
    }
}


Qt::Orientations DesQUI::FlexiLayout::expandingDirections() const {
    return Qt::Horizontal | Qt::Vertical;
}


bool DesQUI::FlexiLayout::hasHeightForWidth() const {
    return true;
}


int DesQUI::FlexiLayout::heightForWidth( int width ) const {
    return doLayout( QRect( 0, 0, width, 0 ), true );
}


QRect DesQUI::FlexiLayout::geometry() const {
    return mGeometry;
}


void DesQUI::FlexiLayout::setGeometry( const QRect& rect ) {
    if ( mGeometry.width() and mGeometry.width() == rect.width() ) {
        return;
    }

    mGeometry = rect;

    QLayout::setGeometry( rect );
    doLayout( rect, false );
}


QSize DesQUI::FlexiLayout::sizeHint() const {
    int height = doLayout( mGeometry, true );

    return QSize( mGeometry.width(), height );
}


QSize DesQUI::FlexiLayout::minimumSize() const {
    QSize size;

    for ( QLayoutItem *item: itemList ) {
        size = size.expandedTo( item->minimumSize() );
    }

    QMargins cMargins( contentsMargins() );

    size += QSize( cMargins.left() + cMargins.right(), cMargins.top() + cMargins.bottom() );

    return size;
}


int DesQUI::FlexiLayout::doLayout( const QRect& rect, bool testOnly ) const {
    if ( not rect.width() ) {
        return 0;
    }

    int left, top, right, bottom;

    getContentsMargins( &left, &top, &right, &bottom );

    QRect effectiveRect = rect.adjusted( +left, +top, -right, -bottom );

    int spaceX = horizontalSpacing();
    int spaceY = verticalSpacing();

    int x = effectiveRect.x();
    int y = effectiveRect.y();

    // for( QLayoutItem *item: itemList ) {
    // item->setGeometry( QRect( QPoint( x, y ), item->sizeHint() ) );
    // y += item->sizeHint().height() + spaceY;
    // }

    // return y - spaceY + bottom;

    int row = 0;

    QMap<int, QLayoutItemList> newLayoutMap;

    /* Create the layout map: Assume all widgets are well-behaved */
    for ( QLayoutItem *item: itemList ) {
        /* If the right end of the widget is outside the rect, place it into the next row */
        if ( x + item->sizeHint().width() > effectiveRect.right() ) {
            /*
             * Increment the row only when the widget is placed away from the left border
             * Also reset x
             */
            if ( x > effectiveRect.x() ) {
                row++;
                x = effectiveRect.x();
            }
        }

        if ( not newLayoutMap.contains( row ) ) {
            newLayoutMap[ row ] = QLayoutItemList();
        }

        newLayoutMap[ row ] << item;

        x += item->sizeHint().width() + spaceX;
    }

    /* We need only the heights */
    if ( testOnly ) {
        int height = top + bottom;
        for ( int row: newLayoutMap.keys() ) {
            int maxH = 0;
            for ( QLayoutItem *item: newLayoutMap[ row ] ) {
                maxH = (item->sizeHint().height() > maxH ? item->sizeHint().height() : maxH);
            }

            height += maxH;
        }

        /* We now have to account for spaceY */
        height += (newLayoutMap.keys().count() - 1) * spaceY;
        return height;
    }

    layoutMap = QMap( newLayoutMap );

    /* Perform the actual laying out */

    y = effectiveRect.y();
    for ( int row: newLayoutMap.keys() ) {
        x = effectiveRect.x();

        /* Get the total width of the row */
        int width = 0, height = 0;
        for ( QLayoutItem *item: newLayoutMap[ row ] ) {
            width += item->sizeHint().width();
            height = std::max( height, item->sizeHint().height() );
        }

        width += (newLayoutMap[ row ].count() - 1) * spaceY;

        int extra = 0;

        /* If the calculated width is less than effectiveRect's width */
        if ( width < effectiveRect.width() ) {
            extra = effectiveRect.width() - width;
        }

        /* Stretch all the items */
        switch ( mLayoutMode ) {
            case DesQUI::FlexiLayout::LeftAligned: {
                /* Pad all the extra space to the right */

                for ( QLayoutItem *item: newLayoutMap[ row ] ) {
                    QSize itemSize( item->sizeHint() );

                    /* Set the geometry of the item */
                    item->setGeometry( QRect( QPoint( x, y ), itemSize ) );

                    /* Increment the x value */
                    x += itemSize.width() + spaceX;
                }

                break;
            }

            case DesQUI::FlexiLayout::Centered: {
                /* Pad half the extra space to the left */
                int pad = extra / 2;
                x += pad;

                for ( QLayoutItem *item: newLayoutMap[ row ] ) {
                    QSize itemSize( item->sizeHint() );

                    /* Set the geometry of the item */
                    item->setGeometry( QRect( QPoint( x, y ), itemSize ) );
                    // qDebug() << "Row" << row << item << "Rect:"
                    // << QString( "(%1, %2)  %3 x %4" ).arg( x ).arg( y ).arg( itemSize.width() ).arg(
                    // itemSize.height() );

                    /* Increment the x value */
                    x += itemSize.width() + spaceX;
                }

                break;
            }

            case DesQUI::FlexiLayout::RightAligned: {
                /* Pad all the extra space to the left */
                x += extra;

                for ( QLayoutItem *item: newLayoutMap[ row ] ) {
                    QSize itemSize( item->sizeHint() );

                    /* Set the geometry of the item */
                    item->setGeometry( QRect( QPoint( x, y ), itemSize ) );

                    /* Increment the x value */
                    x += itemSize.width() + spaceX;
                }

                break;
            }

            case DesQUI::FlexiLayout::Stretched: {
                int stretchable = 0;
                for ( QLayoutItem *item: newLayoutMap[ row ] ) {
                    stretchable += (item->expandingDirections().testFlag( Qt::Horizontal ) ? 1 : 0);
                }

                int dx = (stretchable ? extra / stretchable : 0);
                for ( QLayoutItem *item: newLayoutMap[ row ] ) {
                    QSize itemSize( item->sizeHint() );

                    /* Expand the item if it can be expanded */
                    if ( item->expandingDirections().testFlag( Qt::Horizontal ) ) {
                        itemSize += QSize( dx, 0 );
                    }

                    /* Set the geometry of the item */
                    item->setGeometry( QRect( QPoint( x, y ), itemSize ) );

                    /* Increment the x value */
                    x += itemSize.width() + spaceX;
                }

                break;
            }
        }

        /* We've finished laying out the the current row, time for the next */
        y += height + spaceY;
    }

    mGeometry = rect;
    mGeometry.setHeight( y - spaceY + bottom );

    return y - spaceY + bottom;
}


int DesQUI::FlexiLayout::smartSpacing( QStyle::PixelMetric pm ) const {
    QObject *parent = this->parent();

    if ( !parent ) {
        return -1;
    }

    else if ( parent->isWidgetType() ) {
        QWidget *pw = qobject_cast<QWidget *>( parent );
        return pw->style()->pixelMetric( pm, 0, pw );
    }

    else {
        return static_cast<QLayout *>(parent)->spacing();
    }
}
