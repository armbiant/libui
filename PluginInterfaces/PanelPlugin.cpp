/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include  <QPainter>
#include  <QEnterEvent>
#include  <QMouseEvent>
#include  <QPaintEvent>
#include  <QWheelEvent>

#include "PanelPlugin.hpp"

DesQ::Plugin::PanelInterface::PanelInterface() {
    //
}


DesQ::Plugin::PanelInterface::~PanelInterface() {
    //
}


DesQ::Panel::PluginWidget::PluginWidget( QWidget *parent ) : QWidget( parent ) {
    setMouseTracking( true );
}


DesQ::Panel::PluginWidget::~PluginWidget() {
    // No pointers to be deleted
}


void DesQ::Panel::PluginWidget::setPanelHeight( int height ) {
    panelHeight = height;
    setFixedHeight( height );
}


void DesQ::Panel::PluginWidget::mousePressEvent( QMouseEvent *mpEvent ) {
    if ( mpEvent->button() == Qt::LeftButton ) {
        mPressed = true;
        emit pressed();
    }

    QWidget::mousePressEvent( mpEvent );
}


void DesQ::Panel::PluginWidget::mouseReleaseEvent( QMouseEvent *mrEvent ) {
    if ( (mrEvent->button() == Qt::LeftButton) and mPressed ) {
        mPressed = false;
        emit released();
        emit clicked();
    }

    QWidget::mouseReleaseEvent( mrEvent );
}


void DesQ::Panel::PluginWidget::wheelEvent( QWheelEvent *wEvent ) {
    QPoint numPixels  = wEvent->pixelDelta();
    QPoint numDegrees = wEvent->angleDelta() / 8;

    if ( !numPixels.isNull() ) {
        emit scrolled( numPixels );
    }

    else if ( !numDegrees.isNull() ) {
        QPoint numSteps = numDegrees / 15;
        emit   scrolled( numSteps );
    }

    QWidget::wheelEvent( wEvent );
}


#if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
void DesQ::Panel::PluginWidget::enterEvent( QEvent *event ) {
#else
void DesQ::Panel::PluginWidget::enterEvent( QEnterEvent *event ) {
#endif
    emit entered();

    QWidget::enterEvent( event );
    mInside = true;

    repaint();
}


void DesQ::Panel::PluginWidget::leaveEvent( QEvent *event ) {
    emit exited();

    QWidget::leaveEvent( event );
    mInside = false;

    repaint();
}


void DesQ::Panel::PluginWidget::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );

    QColor highlight = palette().color( QPalette::Highlight );
    QColor border    = highlight;
    QColor backlight = highlight;

    if ( mInside ) {
        border.setAlphaF( 0.1 );
        backlight.setAlphaF( 0.1 );
    }

    else {
        border.setAlphaF( 0.0 );
        backlight.setAlphaF( 0.0 );
    }

    painter.setPen( border );
    painter.setBrush( backlight );
    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3.0, 3.0 );
    painter.end();

    QWidget::paintEvent( pEvent );
}
