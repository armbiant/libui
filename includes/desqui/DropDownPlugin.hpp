/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QObject>
#include <QString>
#include <QStringList>
#include <QWidget>

namespace DesQ {
    namespace Plugin {
        class DropDown;
    }
}

class DesQ::Plugin::DropDown {
    public:
        explicit DropDown();

        virtual ~DropDown();

        /* Name of the plugin */
        virtual QString name() = 0;

        /* Icon for the plugin */
        virtual QIcon icon() = 0;

        /* The plugin version */
        virtual QString version() = 0;

        /* The QWidget */
        virtual QWidget *widget( QWidget *parent ) = 0;
};

Q_DECLARE_INTERFACE( DesQ::Plugin::DropDown, "org.DesQ.Plugin.DropDown" );
