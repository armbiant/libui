/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <desqui/UI.hpp>

namespace DesQUI {
    class ComboBox;
}

class DesQUI::ComboBox : public DesQUI::Widget {
    Q_OBJECT;

    public:
        ComboBox( QString label, QStringList items, bool compact, QWidget *parent );

        bool isCompactMode() override;
        void setCompactMode( bool yes ) override;

        void addItem( QIcon icon = QIcon(), QString name = QString(), QVariant userData = QVariant() );
        void addItems( QStringList );

        void removeItem( int );

        QString currentText();
        QIcon currentIcon();
        int currentIndex();
        QVariant currentData();

        QString itemText( int );
        QIcon itemIcon( int );
        QVariant itemData( int );

    private:
        bool mCompactMode = false;

        QLabel *titleLbl;
        QComboBox *edit;
};
