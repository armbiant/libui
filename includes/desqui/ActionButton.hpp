/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <desqui/UI.hpp>

namespace DesQUI {
    class ActionButton;
}

class DesQUI::ActionButton : public QWidget {
    Q_OBJECT;

    public:
        ActionButton( QString name, QIcon icon, QString target );

        /** Add an extra action to the action button */
        void addAction( QString name, QIcon icon, QString context, QString target );

        /** Add an extra action to the action button */
        void setContexts( QString );

    private:
        QStringList droppedNodes;

        QToolButton *mMainBtn;
        QString mMainTarget;
        QStringList mContexts;

        typedef struct _action {
            QString name;
            QIcon   icon;
            QString target;
        } Action;
        typedef QList<Action> ActionList;
        QMap<QString, ActionList> mContextActionMap;
        QList<QToolButton *> actionBtns;
        QList<QLabel *> actionLbls;

        QFormLayout *mLyt;

        void handleClick();
        void reset();

    Q_SIGNALS:
        void triggered( QString );
};
