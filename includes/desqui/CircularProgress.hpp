/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <desqui/UI.hpp>

/**
 * DesQCircularProgress Base class
 *
 * DesQCircularProgress is designed to show the progress in a circular fashion. This is
 * a purely virtual class. You may probably be looking for DesQUsageGauge/Pie.
 */

namespace DesQUI {
    class CircularProgress;
}

class DesQUI::CircularProgress : public QWidget {
    Q_OBJECT

    public:
        enum Units {
            PERCENT = 0x218628,    // Percentage (8 chars: XXX.XX %)
            SIZE,                  // Size, like KiB, TiB, etc  (10 chars: XXX.XX YiB)
            SPEED                  // Speed, like MiB/s (12 chars: XXX.XX YiB/s)
        };

        CircularProgress( QWidget *parent );

        QSize sizeHint();
        QSize minimumSizeHint();

        int minimumWidth();
        int minimumHeight();

        void setRadius( int );
        void setFixedRadius( int );
        void setMinimumRadius( int );
        void setMaximumRadius( int );

        /* Override width functions */
        void setFixedWidth( int ) { qDebug() << "Use setFixedRadius(...) instead"; }
        void setMinimumWidth( int ) { qDebug() << "Use setMinimumRadius(...) instead"; }
        void setMaximumWidth( int ) { qDebug() << "Use setMaximumRadius(...) instead"; }

        /* Override height functions */
        void setFixedHeight( int ) { qDebug() << "Use setFixedRadius(...) instead"; }
        void setMinimumHeight( int ) { qDebug() << "Use setMinimumRadius(...) instead"; }
        void setMaximumHeight( int ) { qDebug() << "Use setMaximumRadius(...) instead"; }

        QSize size() const;
        void resize( int w, int h );
        void resize( const QSize& size );

        /* Override fixed-size functions */
        void setFixedSize( QSize ) { qDebug() << "Use setFixedRadius(...) instead"; }
        void setMinimumSize( QSize ) { qDebug() << "Use setMinimumRadius(...) instead"; }
        void setMaximumSize( QSize ) { qDebug() << "Use setMaximumRadius(...) instead"; }

        void setFont( const QFont& font );

    protected:
        qint64 mRadius;
        qint64 mMinRadius;
        qint64 mMaxRadius = 0;

        int labelWidth = 0;

        QFontMetrics *fm;

        void paintEvent( QPaintEvent * );
};
