/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

// Local Headers
#include <desqui/UI.hpp>

namespace DesQUI {
    class ActionBar;
    class ActionButton;
    class MainWindow;
}

class DesQUI::MainWindow : public QMainWindow {
    Q_OBJECT

    public:
        MainWindow( QString app, QWidget *parent = nullptr );
        ~MainWindow();

        /** Main Window */
        void setMainView( QWidget * );

        /** Add action bar */
        void createActionBar( Qt::Orientation );

        /** Application Title */
        void setAppTitle( QString );

        /** Application icon */
        void setAppIcon( QString );
        void setAppIcon( QIcon );

        /**Add items to the titlebar */
        void addTitleBarWidget( QWidget *, int );

        /** Add a floating action to the main view */
        void setActionButton( QString name, QIcon icon, QString target );
        void setActionContexts( QString contexts );
        void addActionButtonAction( QString name, QIcon icon, QString context, QString target );

        /**Add items to actionBar */
        int addAction( int page, QString name, QIcon icon, QString target, QString tooltip, bool checkable, bool checked );
        void addSpace( int page, bool stretch );
        void addSeparator( int page );

        /**Change icon */
        void setIcon( int page, int item, QIcon icon );

        /**Enable/disable actions */
        void setActionEnabled( int page, int item );
        void setActionDisabled( int page, int item );

        /**Event filter for special action filtering */
        bool eventFilter( QObject *obj, QEvent *event ) override;

    private:
        void createUI();

        QWidget *mainView            = nullptr;
        QWidget *titleBar            = nullptr;
        DesQUI::ActionBar *actionBar = nullptr;

        DesQUI::ActionButton *mActionBtn = nullptr;

        QGridLayout *mainLyt = nullptr;
        QString mAppName;

        QToolButton *iconBtn = nullptr;
        QToolButton *quitBtn = nullptr;

        QLabel *iconLbl  = nullptr;
        QLabel *titleLbl = nullptr;

        QSizeGrip *resizeGrip = nullptr;

        bool mobileUI;

        /**Show/hide the side actionBar */
        void toggleActionBar();

        /**Check if our UI is in compact mode. Otherwise maximize/restore window */
        void tryMaximize();

    protected:
        void closeEvent( QCloseEvent *cEvent );

    Q_SIGNALS:
        void actionRequested( QString );
};
