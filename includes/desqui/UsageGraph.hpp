/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <desqui/UI.hpp>

/*
 *
 * DesQUsageGraph class DESQUI_DLLSPEC
 *
 * DesQUsageGraph is designed to show the current DesQUsage of various system resources
 * like CPU, RAM, Swap, Network etc.
 *
 * A single DesQUsageGraph can show multiple data: 4 graphs for quad core CPU,
 * two graphs for Network ( Up and Down ), and so on...
 *
 */

typedef QList<double> DesQUsageValues;

namespace DesQUI {
    class UsageGraph;
    class UsagePlot;
}

class DesQUI::UsagePlot : public QWidget {
    Q_OBJECT;

    public:
        UsagePlot( int resources, QWidget *parent );

        /* Y-Range for the graphs */
        void setYRange( double min, double max );

        /* Values for each segment, use absolute values */
        void addValues( DesQUsageValues );

    private:
        int mResources;
        QHash<int, DesQUsageValues> mResValMap;

        double minY = 0;
        double maxY = 100;

    protected:
        void paintEvent( QPaintEvent * );
};

class DesQUI::UsageGraph : public QWidget {
    Q_OBJECT;

    public:
        /* A maximum of 16 resources can be plotted ( say for a 16 core processor ) */
        UsageGraph( QString title, QString unit, int resources, QWidget *parent );

        /* Y-Range for the graphs */
        void setYRange( double min, double max );

        /* Legend for the graphs */
        void setLegend( QStringList );

        /* Hook to add values to DesQUsagePlot */
        void addValues( DesQUsageValues );

    private:
        int mResources;

        QString mTitle = "Graph";
        QString mUnits = "%";

        double minY = 0;
        double maxY = 100;

        QLabel *mTitleLbl;
        QList<QLabel *> yLabels;
        QList<QLabel *> xLabels;
        QList<QLabel *> legends;

        DesQUI::UsagePlot *plot;

    protected:
        void resizeEvent( QResizeEvent * );
};
