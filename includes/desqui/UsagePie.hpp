/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <desqui/UI.hpp>
#include <desqui/CircularProgress.hpp>

/*
 *
 * DesQUsagePie class
 *
 * DesQUsagePie is designed to show the current DesQUsage of various system resources
 * like RAM, HDD, etc...
 *
 * A single DesQUsagePie can show data in multiple segments: Usage of 4 partitions of a HDD
 * or usage of RAM and Swap, and so on..
 *
 */

typedef struct pie_resource_value_t {
    qint64 total;
    qint64 used;
} PieResourceValue;
typedef QList<PieResourceValue> PieResourceValues;

namespace DesQUI {
    class CircularProgress;
    class UsagePie;
}

class DesQUI::UsagePie : public DesQUI::CircularProgress {
    Q_OBJECT;

    public:
        UsagePie( QString resource, int segments, QWidget *parent );

        /* Units */
        void setUnits( DesQUI::CircularProgress::Units units );

        /* Labels for the legends */
        void setLabels( QStringList );

        /* Set the pie values */
        void setValues( PieResourceValues );

    private:
        int mSegments;
        DesQUI::UsagePie::Units mUnits;
        qint64 sizeHint;
        QString resName;

        qint64 mMax;

        PieResourceValues mValues;
        QStringList mLabels;

        int labelHeight = 0;

    protected:
        void paintEvent( QPaintEvent * );
};
