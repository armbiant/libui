/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <desq/Utils.hpp>
#include "desqui/UsageGauge.hpp"

static QList<QColor> colors = {
    0x1ABC9C, 0x9B59B6, 0xFF8F00, 0xE74C3C, 0x4E342E,
    0x8E44AD, 0xEF6C00, 0xC0392B, 0x34495E, 0x424242,
    0x5499C7, 0xCD6155, 0xF5B041, 0x566573, 0x58D68D
};

DesQUI::UsageGauge::UsageGauge( QString resource, int segments, QWidget *parent ) : DesQUI::CircularProgress( parent ) {
    mSegments = segments > 5 ? 5 : segments;

    if ( segments > 5 ) {
        qWarning() << "DesQUI::UsageGauge for" << resource << "will show only 5 indicators instead of" << segments;
    }

    mUnits  = DesQUI::CircularProgress::PERCENT;
    resName = QString( resource );

    setRadius( 50 );

    setSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding );

    for ( int i = 0; i < segments; i++ ) {
        mValues.push_back( 0 );
    }
}


void DesQUI::UsageGauge::setMaximum( qint64 max ) {
    mMax = max;

    repaint();
}


void DesQUI::UsageGauge::setUnits( DesQUI::UsageGauge::Units units ) {
    mUnits = units;
    repaint();
}


void DesQUI::UsageGauge::setLabels( QStringList labels ) {
    mLabels.clear();
    mLabels << labels;

    QFontMetrics fm( font() );

    labelHeight = fm.lineSpacing() * labels.count() + (labels.count() - 1) * 5;

    repaint();
}


void DesQUI::UsageGauge::setValues( GaugeResourceValues values ) {
    mValues.clear();
    qint64 max = 0;

    for ( qint64 val: values ) {
        mValues.push_back( val );

        if ( val > max ) {
            max = val;
        }
    }

    if ( max >= DesQ::Utils::SizeHint::TiB ) {
        sizeHint = DesQ::Utils::SizeHint::TiB;
    }
    else if ( max >= DesQ::Utils::SizeHint::GiB ) {
        sizeHint = DesQ::Utils::SizeHint::GiB;
    }
    else if ( max >= DesQ::Utils::SizeHint::MiB ) {
        sizeHint = DesQ::Utils::SizeHint::MiB;
    }
    else if ( max >= DesQ::Utils::SizeHint::KiB ) {
        sizeHint = DesQ::Utils::SizeHint::KiB;
    }
    else {
        sizeHint = DesQ::Utils::SizeHint::Auto;
    }

    repaint();
}


void DesQUI::UsageGauge::paintEvent( QPaintEvent *pEvent ) {
    /* Side will be twice the radius */
    int    side = mRadius * 2;
    QImage img( side, side, QImage::Format_ARGB32 );

    img.fill( Qt::transparent );

    QPainter painter( &img );

    painter.setRenderHints( QPainter::Antialiasing | QPainter::TextAntialiasing );

    painter.save();
    QPalette pltt = qApp->palette();
    QColor   bg   = pltt.color( QPalette::Highlight );

    bg.setAlpha( 27 );
    painter.setPen( Qt::NoPen );
    painter.setBrush( bg );
    painter.drawEllipse( QPointF( mRadius, mRadius ), mRadius - 1, mRadius - 1 );
    painter.restore();

    /* Start at 7 o'clock */
    int startAngle = -135 * 16;

    double penWidth = 5.0;

    /* Draw the finish line */
    painter.save();
    QLineF angleLine;

    angleLine.setP1( QPointF( mRadius, mRadius ) );
    angleLine.setAngle( -45.0 );
    angleLine.setLength( 20 );
    angleLine.translate( mRadius - 40, mRadius - 40 );
    painter.setPen( QPen( pltt.color( QPalette::Highlight ), 2.0, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
    painter.drawLine( angleLine );
    painter.restore();

    int x = 2;
    int y = 2;

    for ( int s = 0; s < mSegments; s++ ) {
        painter.save();

        painter.setPen( QPen( colors.at( s ), penWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
        double spanAngle = DesQ::Utils::formatSizeRaw( mValues.at( s ), sizeHint ) * -270.0 * 16.0 / mMax;

        x += penWidth + 2;
        y += penWidth + 2;

        painter.drawArc( QRect( x, y, side - 2 * x, side - 2 * y ), startAngle, spanAngle );
        painter.restore();
    }

    QString text;

    switch ( mUnits ) {
        case DesQUI::CircularProgress::PERCENT: {
            text = "%";
            break;
        }

        case DesQUI::CircularProgress::SIZE: {
            text = DesQ::Utils::formatSizeStr( 0, sizeHint );

            break;
        }

        case DesQUI::CircularProgress::SPEED: {
            text = DesQ::Utils::formatSizeStr( 0, sizeHint ) + "/s";

            break;
        }
    }

    // Centeral circle
    painter.setPen( Qt::NoPen );
    painter.setBrush( pltt.color( QPalette::HighlightedText ).darker() );
    painter.drawEllipse( QPoint( mRadius, mRadius ), 25, 25 );

    painter.save();
    painter.setPen( pltt.color( QPalette::HighlightedText ) );
    painter.setFont( QFont( font().family(), 8 ) );
    painter.drawText( QRectF( 0, 0, side, side ), Qt::AlignCenter, resName + "\n" + text );
    painter.restore();
    painter.end();

    painter.begin( this );
    painter.setRenderHints( QPainter::Antialiasing );

    /* Background rectangle */
    painter.save();
    painter.setPen( Qt::NoPen );
    painter.setBrush( QColor( 0, 0, 0, 10 ) );
    painter.drawRoundedRect( rect(), 3, 3 );
    painter.setBrush( QColor( 255, 255, 255, 10 ) );
    painter.drawRoundedRect( rect(), 3, 3 );
    painter.restore();

    /* Resource */
    painter.drawImage( QRect( 5, (height() - side) / 2, side, side ), img );

    /* Labels */
    painter.save();
    y = (height() - labelHeight) / 2 - 5;           // We will be adding 5, so reduce that amount at first.
    y = (y > 0 ? y : 0);
    QRectF bound( 0, y, 0, 0 );

    for ( int s = 0; s < mLabels.count(); s++ ) {
        painter.setPen( colors.value( s ) );
        painter.drawText( QRect( side + 10, bound.bottom() + 5, labelWidth, height() ), Qt::AlignTop | Qt::AlignRight, mLabels.value( s ), &bound );
    }
    painter.restore();

    painter.end();

    pEvent->accept();
}
