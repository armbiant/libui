/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <desqui/UI.hpp>

namespace DesQUI {
    class ActionBarItem;
}

namespace DFL {
    class DynamicLayout;
}

typedef struct ActionBar_Page_t {
    DFL::DynamicLayout             *layout;
    QList<DesQUI::ActionBarItem *> items;

    QSize pageSize();
} ActionBarPage;


class Expander : public QToolButton {
    Q_OBJECT;

    public:
        Expander( Qt::Orientation orient, QWidget * );

        void setExpanded( bool );

    private:
        Qt::Orientation mOrient;
        QPixmap icon;

    protected:
        void paintEvent( QPaintEvent *pEvent );
};


class ActionBarSpace : public QWidget {
    Q_OBJECT;

    public:
        ActionBarSpace( Qt::Orientation orient, QWidget *parent );
};


class ActionBarLongSpace : public QWidget {
    Q_OBJECT;

    public:
        ActionBarLongSpace( Qt::Orientation orient, QWidget *parent );
};


class ActionBarSeparator : public QWidget {
    Q_OBJECT;

    public:
        ActionBarSeparator( Qt::Orientation orient, QWidget *parent );

    protected:
        void paintEvent( QPaintEvent *pEvent );
};
