/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "MainWindow.hpp"
#include "ActionBar.hpp"
#include "ActionButton.hpp"

DesQUI::MainWindow::MainWindow( QString app, QWidget *parent ) : QMainWindow( parent ) {
    /* Mobile interface: Start as false */
    mobileUI = false;

    /* App Name */
    mAppName = app;

    /** Initialize actionBar to nullptr */
    actionBar = nullptr;

    /* Install the event filter */
    createUI();

    /* Install the event filter to catch the titlebar double click */
    titleBar->installEventFilter( this );
    installEventFilter( this );
}


DesQUI::MainWindow::~MainWindow() {
    if ( mainView ) {
        delete mainView;
    }

    if ( titleBar ) {
        delete titleBar;
    }

    if ( actionBar ) {
        delete actionBar;
    }

    if ( mActionBtn ) {
        delete mActionBtn;
    }

    if ( mainLyt ) {
        delete mainLyt;
    }

    if ( iconBtn ) {
        delete iconBtn;
    }

    if ( quitBtn ) {
        delete quitBtn;
    }

    if ( iconLbl ) {
        delete iconLbl;
    }

    if ( titleLbl ) {
        delete titleLbl;
    }

    if ( resizeGrip ) {
        delete resizeGrip;
    }
}


void DesQUI::MainWindow::createUI() {
    /* TitleBar */
    titleBar = new QWidget();
    titleBar->setObjectName( "TitleBar" );
    titleBar->setFixedHeight( 32 );

    QPalette tPlt( palette() );

    tPlt.setColor( QPalette::Window, tPlt.color( QPalette::Window ).darker( 110 ) );
    titleBar->setPalette( tPlt );

    /* App Icon */
    if ( mobileUI ) {
        iconBtn = new QToolButton();
        iconBtn->setIcon( QIcon::fromTheme( "desq" ) );
        iconBtn->setIconSize( QSize( 24, 24 ) );
        iconBtn->setFixedSize( QSize( 32, 32 ) );
        iconBtn->setFocusPolicy( Qt::NoFocus );
        iconBtn->setAutoRaise( true );
        connect( iconBtn, &QToolButton::clicked, this, &DesQUI::MainWindow::toggleActionBar );
    }

    else {
        iconLbl = new QLabel();
        iconLbl->setFixedSize( QSize( 32, 32 ) );
        iconLbl->setPixmap( QIcon::fromTheme( "desq" ).pixmap( 24 ) );
        iconLbl->setAlignment( Qt::AlignCenter );

        titleBar->hide();
    }

    /* App Label */
    titleLbl = new QLabel( "DesQ Main Window" );
    titleLbl->setFont( QFont( font().family(), 10, QFont::Bold ) );

    /* Close Button */
    quitBtn = new QToolButton();
    quitBtn->setIcon( QIcon::fromTheme( "window-close" ) );
    quitBtn->setIconSize( QSize( 24, 24 ) );
    quitBtn->setFixedSize( QSize( 32, 32 ) );
    quitBtn->setFocusPolicy( Qt::NoFocus );
    quitBtn->setAutoRaise( true );
    connect( quitBtn, &QToolButton::clicked, this, &DesQUI::MainWindow::close );

    QHBoxLayout *titleLyt = new QHBoxLayout();

    titleLyt->setContentsMargins( QMargins() );
    titleLyt->setSpacing( 0 );

    if ( mobileUI ) {
        titleLyt->addWidget( iconBtn );
    }

    else {
        titleLyt->addWidget( iconLbl );
    }

    titleLyt->addStretch();
    titleLyt->addWidget( titleLbl );
    titleLyt->addStretch();
    titleLyt->addWidget( quitBtn );
    titleBar->setLayout( titleLyt );

    /* Main View Dummy */
    mainView = new QWidget( this );
    mainView->setMinimumSize( QSize( 300, 300 ) );
    mainView->setSizePolicy( QSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding ) );

    /* Main Layout */
    mainLyt = new QGridLayout();
    mainLyt->setContentsMargins( QMargins() );
    mainLyt->setSpacing( 0 );

    mainLyt->addWidget( titleBar, 0, 0 );
    mainLyt->addWidget( mainView, 1, 0 );

    QWidget *base = new QWidget();

    base->setLayout( mainLyt );
    setCentralWidget( base );
}


/* Main Window */
void DesQUI::MainWindow::setMainView( QWidget *view ) {
    mainLyt->removeWidget( mainView );
    delete mainView;

    mainView = view;
    mainLyt->addWidget( mainView, 1, 0 );

    view->lower();
}


/* Action Bar */
void DesQUI::MainWindow::createActionBar( Qt::Orientation orient ) {
    if ( actionBar ) {
        return;
    }

    /* ActionBar */
    actionBar = new DesQUI::ActionBar( this, orient );
    connect( actionBar, &DesQUI::ActionBar::action, this, &DesQUI::MainWindow::actionRequested );

    QHBoxLayout *actLyt = new QHBoxLayout();

    actLyt->setContentsMargins( QMargins( 5, 5, 5, 5 ) );
    actLyt->addWidget( actionBar );

    mainLyt->addLayout( actLyt, 1, 0, Qt::AlignTop | Qt::AlignLeft );
}


/* Application Title */
void DesQUI::MainWindow::setAppTitle( QString title ) {
    setWindowTitle( title );
    titleLbl->setText( title );
}


/* Application icon */
void DesQUI::MainWindow::setAppIcon( QString icoStr ) {
    if ( mobileUI ) {
        iconBtn->setIcon( QIcon::fromTheme( icoStr, QIcon( icoStr ) ) );
    }

    else {
        iconLbl->setPixmap( QIcon::fromTheme( icoStr, QIcon( icoStr ) ).pixmap( 24 ) );
    }
}


void DesQUI::MainWindow::setAppIcon( QIcon icon ) {
    if ( mobileUI ) {
        iconBtn->setIcon( icon );
    }

    else {
        iconLbl->setPixmap( icon.pixmap( 24 ) );
    }
}


/* Add items to titlebar */
void DesQUI::MainWindow::addTitleBarWidget( QWidget *widget, int pos ) {
    Q_UNUSED( widget );     // -> Widget to be added to the title bar
    Q_UNUSED( pos );        // -> Position to add the widget (0 = left, next to the app icon and 1 = right,
                            // next to the close button)
}


/* Add floating action to the main view */
void DesQUI::MainWindow::setActionButton( QString name, QIcon icon, QString target ) {
    mActionBtn = new DesQUI::ActionButton( name, icon, target );

    /* Create the connection */
    connect(
        mActionBtn, &DesQUI::ActionButton::triggered, [ = ] ( QString tgt ) {
            emit actionRequested( tgt );
        }
    );

    /* Add it to the layout */
    mainLyt->addWidget( mActionBtn, 1, 0, Qt::AlignRight | Qt::AlignBottom );
}


void DesQUI::MainWindow::setActionContexts( QString contexts ) {
    /**
     * If the action button has not been initialized,
     * ignore setting contexts.
     */
    if ( not mActionBtn ) {
        return;
    }

    mActionBtn->setContexts( contexts );
}


/* Add floating action to the main view */
void DesQUI::MainWindow::addActionButtonAction( QString name, QIcon icon, QString context, QString target ) {
    /**
     * If the action button has not been initialized,
     * ignore actions added to it.
     */
    if ( not mActionBtn ) {
        return;
    }

    mActionBtn->addAction( name, icon, context, target );
}


/* Add items to actionBar */
int DesQUI::MainWindow::addAction( int page, QString name, QIcon icon, QString target, QString tooltip, bool checkable, bool checked ) {
    if ( not actionBar ) {
        return -1;
    }

    while ( actionBar->pageCount() < page + 1 ) {
        actionBar->addPage();
    }

    return actionBar->addAction( page, name, icon, target, tooltip, checkable, checked );
}


/* Add space/stretch to actionBar */
void DesQUI::MainWindow::addSpace( int page, bool stretch ) {
    if ( not actionBar ) {
        return;
    }

    while ( actionBar->pageCount() < page + 1 ) {
        actionBar->addPage();
    }

    if ( stretch ) {
        actionBar->addStretch( page );
    }

    else {
        actionBar->addSpace( page );
    }
}


void DesQUI::MainWindow::addSeparator( int page ) {
    if ( not actionBar ) {
        return;
    }

    while ( actionBar->pageCount() < page + 1 ) {
        actionBar->addPage();
    }

    actionBar->addSeparator( page );
}


void DesQUI::MainWindow::setIcon( int page, int item, QIcon icon ) {
    if ( not actionBar ) {
        return;
    }

    actionBar->setIcon( page, item, icon );
}


void DesQUI::MainWindow::setActionEnabled( int page, int item ) {
    if ( not actionBar ) {
        return;
    }

    actionBar->setItemEnabled( page, item );
}


void DesQUI::MainWindow::setActionDisabled( int page, int item ) {
    if ( not actionBar ) {
        return;
    }

    actionBar->setItemDisabled( page, item );
}


/* Even filtering to get title bar double press */
bool DesQUI::MainWindow::eventFilter( QObject *obj, QEvent *event ) {
    /** Object based */
    if ( obj == titleBar ) {
        if ( event->type() == QEvent::MouseButtonDblClick ) {
            tryMaximize();
            return true;
        }
    }

    /** Event-type based */
    if ( event->type() == QEvent::WindowActivate ) {
        QPalette tPlt( palette() );
        tPlt.setColor( QPalette::Window, tPlt.color( QPalette::Window ).darker( 110 ) );
        titleBar->setAutoFillBackground( true );
        titleBar->setPalette( tPlt );

        return true;
    }

    else if ( event->type() == QEvent::WindowDeactivate ) {
        QPalette tPlt( palette() );
        tPlt.setColor( QPalette::Window, tPlt.color( QPalette::Text ).darker( 120 ) );
        titleBar->setAutoFillBackground( true );
        titleBar->setPalette( tPlt );

        return true;
    }

    return QMainWindow::eventFilter( obj, event );
}


/* This is MobileUI special */
void DesQUI::MainWindow::toggleActionBar() {
    if ( not actionBar ) {
        return;
    }

    /* If the actionBar is visible, hide it and show the mainView */
    if ( actionBar->isVisible() ) {
        actionBar->hide();
    }

    /* Otherwise, show the actionBar and hide the mainView */
    else {
        actionBar->show();
    }
}


void DesQUI::MainWindow::tryMaximize() {
    /* If we are in mobileUI mode, we will be maximized. So, nothing doing. */
    if ( mobileUI ) {
        return;
    }

    if ( isMaximized() ) {
        showNormal();
    }

    else {
        showMaximized();
    }
}


void DesQUI::MainWindow::closeEvent( QCloseEvent *cEvent ) {
    /* An attempt at saving the state and size */
    QSettings sett( "DesQ", mAppName );

    sett.setValue( "Session/Maximized", isMaximized() );

    if ( not isMaximized() ) {
        sett.setValue( "Session/Geometry", geometry() );
    }

    cEvent->accept();
}
