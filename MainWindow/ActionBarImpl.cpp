/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "ActionBarImpl.hpp"
#include <DFL/DF5/DynamicLayout.hpp>

QSize ActionBarPage::pageSize() {
    int width  = 0;
    int height = 0;

    for ( int i = 0; i < layout->count(); i++ ) {
        QLayoutItem *item = layout->itemAt( i );

        if ( item->widget() != nullptr ) {
            width  += item->widget()->width();
            height += item->widget()->height();
        }
    }

    return QSize( width, height );
}


Expander::Expander( Qt::Orientation orient, QWidget *parent ) : QToolButton( parent ) {
    mOrient = orient;

    switch ( orient ) {
        case Qt::Horizontal: {
            icon = QIcon::fromTheme( "arrow-left" ).pixmap( 24 );
            break;
        }

        case Qt::Vertical: {
            icon = QIcon::fromTheme( "arrow-up" ).pixmap( 24 );
            break;
        }
    }

    setAutoRaise( true );
    setFixedSize( DesQUI::ActionBarHeight, DesQUI::ActionBarHeight );
}


void Expander::setExpanded( bool yes ) {
    switch ( mOrient ) {
        case Qt::Horizontal: {
            icon = QIcon::fromTheme( yes ? "arrow-left" : "arrow-right" ).pixmap( 24 );
            break;
        }

        case Qt::Vertical: {
            icon = QIcon::fromTheme( yes ? "arrow-up" : "arrow-down" ).pixmap( 24 );
            break;
        }
    }
}


void Expander::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );

    QColor highlight( palette().color( QPalette::Highlight ) );

    painter.setPen( QPen( highlight, 1.0 ) );

    if ( underMouse() ) {
        highlight.setAlphaF( 0.3 );
    }

    else if ( isDown() ) {
        highlight.setAlphaF( 0.6 );
    }

    else {
        painter.setOpacity( 0.25 );
        highlight.setAlphaF( 0.0 );
    }

    painter.setBrush( highlight );
    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3.0, 3.0 );

    qreal pad = (DesQUI::ActionBarHeight - 24) / 2.0;

    painter.drawPixmap( QPointF( pad, pad ), icon );

    painter.end();
}


ActionBarSpace::ActionBarSpace( Qt::Orientation orient, QWidget *parent ) : QWidget( parent ) {
    switch ( orient ) {
        case Qt::Horizontal: {
            setFixedSize( QSize( 0.5 * DesQUI::ActionBarHeight, DesQUI::ActionBarHeight ) );
            break;
        }

        case Qt::Vertical: {
            setFixedSize( QSize( DesQUI::ActionBarHeight, 0.5 * DesQUI::ActionBarHeight ) );
            break;
        }
    }
}


ActionBarLongSpace::ActionBarLongSpace( Qt::Orientation orient, QWidget *parent ) : QWidget( parent ) {
    switch ( orient ) {
        case Qt::Horizontal: {
            setFixedSize( QSize( 3 * DesQUI::ActionBarHeight, DesQUI::ActionBarHeight ) );
            break;
        }

        case Qt::Vertical: {
            setFixedSize( QSize( DesQUI::ActionBarHeight, 3 * DesQUI::ActionBarHeight ) );
            break;
        }
    }
}


ActionBarSeparator::ActionBarSeparator( Qt::Orientation orient, QWidget *parent ) : QWidget( parent ) {
    switch ( orient ) {
        case Qt::Horizontal: {
            setFixedSize( QSize( 1, DesQUI::ActionBarHeight ) );
            break;
        }

        case Qt::Vertical: {
            setFixedSize( QSize( DesQUI::ActionBarHeight, 1 ) );
            break;
        }
    }
}


void ActionBarSeparator::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setPen( Qt::gray );
    painter.drawRect( rect() );
    painter.end();

    pEvent->accept();
}
