/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "desqui/LineEdit.hpp"

DesQUI::LineEdit::LineEdit( QString label, QString placeHolder, bool compact, QWidget *parent ) : DesQUI::Widget( parent ) {
    titleLbl = new QLabel( label );

    edit = new QLineEdit( this );
    edit->setPlaceholderText( placeHolder );
    edit->setMinimumHeight( 24 );

    if ( label.contains( "&" ) ) {
        titleLbl->setBuddy( edit );
    }

    edit->setStyleSheet( "QLineEdit { border: none; border-bottom: 2px solid gray; background: transparent; } QLineEdit:focus { border-bottom: 2px solid palette(Highlight);}" );

    QBoxLayout *lyt;

    if ( compact ) {
        lyt = new QVBoxLayout();
    }

    else {
        lyt = new QHBoxLayout();
    }

    lyt->setContentsMargins( QMargins() );
    lyt->setSpacing( 0 );
    lyt->addWidget( titleLbl );
    lyt->addWidget( edit );

    setLayout( lyt );

    setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred ) );
}


bool DesQUI::LineEdit::isCompactMode() {
    return mCompactMode;
}


void DesQUI::LineEdit::setCompactMode( bool yes ) {
    mCompactMode = yes;

    qDeleteAll( children() );

    QBoxLayout *lyt;

    if ( yes ) {
        lyt = new QVBoxLayout();
    }

    else {
        lyt = new QHBoxLayout();
    }

    lyt->addWidget( titleLbl );
    lyt->addWidget( edit );

    setLayout( lyt );
}


QString DesQUI::LineEdit::placeholderText() {
    return edit->placeholderText();
}


void DesQUI::LineEdit::setPlaceholderText( QString txt ) {
    edit->setPlaceholderText( txt );
}


QString DesQUI::LineEdit::text() {
    return edit->placeholderText();
}


void DesQUI::LineEdit::setText( QString txt ) {
    edit->setText( txt );
}
