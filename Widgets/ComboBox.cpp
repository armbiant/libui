/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "desqui/ComboBox.hpp"

DesQUI::ComboBox::ComboBox( QString label, QStringList items, bool compact, QWidget *parent ) : DesQUI::Widget( parent ) {
    titleLbl = new QLabel( label );

    edit = new QComboBox( this );
    edit->addItems( items );
    edit->setMinimumHeight( 24 );

    if ( label.contains( "&" ) ) {
        titleLbl->setBuddy( edit );
    }

    edit->setStyleSheet(
        "QComboBox {"
        "\tborder: none;"
        "\tborder-bottom: 2px solid gray;"
        "\tbackground: transparent;"
        "}"
        "QComboBox:focus {"
        "\tborder-bottom: 2px solid palette(Highlight);"
        "}"
        "QComboBox::drop-down {"
        "\tborder: none;"
        "}"
        "QComboBox::down-arrow {"
        "\timage: url('/usr/share/icons/breeze-dark/actions/16/arrow-down.svg');"
        "\theight: 8px;"
        "\twidth: 8px;"
        "}"
    );

    QBoxLayout *lyt;

    if ( compact ) {
        lyt = new QVBoxLayout();
    }

    else {
        lyt = new QHBoxLayout();
    }

    lyt->setContentsMargins( QMargins() );
    lyt->setSpacing( 0 );
    lyt->addWidget( titleLbl );
    lyt->addWidget( edit );

    setLayout( lyt );

    setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred ) );
}


bool DesQUI::ComboBox::isCompactMode() {
    return mCompactMode;
}


void DesQUI::ComboBox::setCompactMode( bool yes ) {
    mCompactMode = yes;

    qDeleteAll( children() );

    QBoxLayout *lyt;

    if ( yes ) {
        lyt = new QVBoxLayout();
    }

    else {
        lyt = new QHBoxLayout();
    }

    lyt->addWidget( titleLbl );
    lyt->addWidget( edit );

    setLayout( lyt );
}


void DesQUI::ComboBox::addItem( QIcon icon, QString name, QVariant userData ) {
    edit->addItem( icon, name, userData );
}


void DesQUI::ComboBox::addItems( QStringList items ) {
    edit->addItems( items );
}


void DesQUI::ComboBox::removeItem( int index ) {
    edit->removeItem( index );
}


QString DesQUI::ComboBox::currentText() {
    return edit->currentText();
}


QIcon DesQUI::ComboBox::currentIcon() {
    return edit->currentData( Qt::DecorationRole ).value<QIcon>();
}


int DesQUI::ComboBox::currentIndex() {
    return edit->currentIndex();
}


QVariant DesQUI::ComboBox::currentData() {
    return edit->currentData( Qt::UserRole );
}


QString DesQUI::ComboBox::itemText( int index ) {
    return edit->itemText( index );
}


QIcon DesQUI::ComboBox::itemIcon( int index ) {
    return edit->itemIcon( index );
}


QVariant DesQUI::ComboBox::itemData( int index ) {
    return edit->itemData( index, Qt::UserRole );
}
