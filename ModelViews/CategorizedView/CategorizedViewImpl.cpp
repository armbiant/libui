/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This file was originally a part of the KDE project: KF5 ItemViews Module
 * 2007, 2009 Rafael Fernández López <ereslibre@kde.org>
 * Original License: LGPL 2.0
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "CategorizedView.hpp"
#include "CategorizedViewImpl.hpp"

#include <QPaintEvent>
#include <QPainter>
#include <QScrollBar>

#include "SortFilterProxyModel.hpp"
#include "CategoryDrawer.hpp"


DesQUI::CategorizedViewPrivate::CategorizedViewPrivate( DesQUI::CategorizedView *qq ) {
    q = qq;

    hoveredBlock    = new Block();
    hoveredIndex    = QModelIndex();
    pressedPosition = QPoint();
    rubberBandRect  = QRect();
}


DesQUI::CategorizedViewPrivate::~CategorizedViewPrivate() {
    delete hoveredBlock;
}


bool DesQUI::CategorizedViewPrivate::isCategorized() const {
    return proxyModel && categoryDrawer && proxyModel->isCategorizedModel();
}


QStyleOptionViewItem DesQUI::CategorizedViewPrivate::blockRect( const QModelIndex& representative ) {
    // Q_D( CategorizedView );

    QStyleOptionViewItem option( q->viewOptions() );
    const int            height          = categoryDrawer->categoryHeight( representative, option );
    const QString        categoryDisplay = representative.data( DesQUI::SortFilterProxyModel::CategoryDisplayRole ).toString();
    QPoint               pos             = blockPosition( categoryDisplay );

    pos.ry() -= height;
    option.rect.setTopLeft( pos );
    option.rect.setWidth( viewportWidth() + categoryDrawer->leftMargin() + categoryDrawer->rightMargin() );
    option.rect.setHeight( height + blockHeight( categoryDisplay ) );
    option.rect = mapToViewport( option.rect );

    return option;
}


QPair<QModelIndex, QModelIndex> DesQUI::CategorizedViewPrivate::intersectingIndexesWithRect( const QRect& _rect ) const {
    // Q_D( const CategorizedView );

    const int rowCount = proxyModel->rowCount();

    const QRect rect = _rect.normalized();

    // binary search to find out the top border
    int bottom = 0;
    int top    = rowCount - 1;

    while ( bottom <= top ) {
        const int         middle   = (bottom + top) / 2;
        const QModelIndex index    = proxyModel->index( middle, q->modelColumn(), q->rootIndex() );
        const QRect       itemRect = q->visualRect( index );

        if ( itemRect.bottomRight().y() <= rect.topLeft().y() ) {
            bottom = middle + 1;
        }
        else {
            top = middle - 1;
        }
    }

    const QModelIndex bottomIndex = proxyModel->index( bottom, q->modelColumn(), q->rootIndex() );

    // binary search to find out the bottom border
    bottom = 0;
    top    = rowCount - 1;
    while ( bottom <= top ) {
        const int         middle   = (bottom + top) / 2;
        const QModelIndex index    = proxyModel->index( middle, q->modelColumn(), q->rootIndex() );
        const QRect       itemRect = q->visualRect( index );

        if ( itemRect.topLeft().y() <= rect.bottomRight().y() ) {
            bottom = middle + 1;
        }
        else {
            top = middle - 1;
        }
    }

    const QModelIndex topIndex = proxyModel->index( top, q->modelColumn(), q->rootIndex() );

    return qMakePair( bottomIndex, topIndex );
}


QPoint DesQUI::CategorizedViewPrivate::blockPosition( const QString& category ) {
    // Q_D( const CategorizedView );

    Block& block = blocks[ category ];

    if ( block.outOfQuarantine && !block.topLeft.isNull() ) {
        return block.topLeft;
    }

    QPoint res( categorySpacing, 0 );

    const QModelIndex index = block.firstIndex;

    for (auto it = blocks.begin(); it != blocks.end(); ++it) {
        Block&            block         = *it;
        const QModelIndex categoryIndex = block.firstIndex;

        if ( index.row() < categoryIndex.row() ) {
            continue;
        }

        res.ry() += categoryDrawer->categoryHeight( categoryIndex, q->viewOptions() ) + categorySpacing;

        if ( index.row() == categoryIndex.row() ) {
            continue;
        }

        res.ry() += blockHeight( it.key() );
    }

    block.outOfQuarantine = true;
    block.topLeft         = res;

    return res;
}


int DesQUI::CategorizedViewPrivate::blockHeight( const QString& category ) {
    // Q_D( const CategorizedView );

    Block& block = blocks[ category ];

    if ( block.collapsed ) {
        return 0;
    }

    if ( block.height > -1 ) {
        return block.height;
    }

    const QModelIndex firstIndex  = block.firstIndex;
    const QModelIndex lastIndex   = proxyModel->index( firstIndex.row() + block.items.count() - 1, q->modelColumn(), q->rootIndex() );
    const QRect       topLeft     = q->visualRect( firstIndex );
    QRect             bottomRight = q->visualRect( lastIndex );

    if ( hasGrid() ) {
        bottomRight.setHeight( qMax( bottomRight.height(), q->gridSize().height() ) );
    }
    else {
        if ( !q->uniformItemSizes() ) {
            bottomRight.setHeight( highestElementInLastRow( block ) + q->spacing() * 2 );
        }
    }

    const int height = bottomRight.bottomRight().y() - topLeft.topLeft().y() + 1;

    block.height = height;

    return height;
}


int DesQUI::CategorizedViewPrivate::viewportWidth() const {
    // Q_D( const CategorizedView );

    return q->viewport()->width() - categorySpacing * 2 - categoryDrawer->leftMargin() - categoryDrawer->rightMargin();
}


void DesQUI::CategorizedViewPrivate::regenerateAllElements() {
    for (QHash<QString, Block>::Iterator it = blocks.begin(); it != blocks.end(); ++it) {
        Block& block = *it;
        block.outOfQuarantine = false;
        block.quarantineStart = block.firstIndex;
        block.height          = -1;
    }
}


void DesQUI::CategorizedViewPrivate::rowsInserted( const QModelIndex& parent, int start, int end ) {
    // Q_D( CategorizedView );

    if ( !isCategorized() ) {
        return;
    }

    for (int i = start; i <= end; ++i) {
        const QModelIndex index = proxyModel->index( i, q->modelColumn(), parent );

        Q_ASSERT( index.isValid() );

        const QString category = categoryForIndex( index );

        Block& block = blocks[ category ];

        // BEGIN: update firstIndex
        // save as firstIndex in block if
        //     - it forced the category creation (first element on this category)
        //     - it is before the first row on that category
        const QModelIndex firstIndex = block.firstIndex;

        if ( !firstIndex.isValid() || (index.row() < firstIndex.row() ) ) {
            block.firstIndex = index;
        }

        // END: update firstIndex

        Q_ASSERT( block.firstIndex.isValid() );

        const int firstIndexRow = block.firstIndex.row();

        block.items.insert( index.row() - firstIndexRow, DesQUI::CategorizedViewPrivate::Item() );
        block.height = -1;

        q->visualRect( index );
        q->viewport()->update();
    }

    // BEGIN: update the items that are in quarantine in affected categories
    {
        const QModelIndex lastIndex = proxyModel->index( end, q->modelColumn(), parent );
        const QString     category  = categoryForIndex( lastIndex );
        DesQUI::CategorizedViewPrivate::Block& block = blocks[ category ];
        block.quarantineStart = block.firstIndex;
    }
    // END: update the items that are in quarantine in affected categories

    // BEGIN: mark as in quarantine those categories that are under the affected ones
    {
        const QModelIndex firstIndex            = proxyModel->index( start, q->modelColumn(), parent );
        const QString     category              = categoryForIndex( firstIndex );
        const QModelIndex firstAffectedCategory = blocks[ category ].firstIndex;
        // BEGIN: order for marking as alternate those blocks that are alternate
        QList<Block> blockList = blocks.values();
        std::sort( blockList.begin(), blockList.end(), Block::lessThan );
        QList<int> firstIndexesRows;
        for (const Block& block : std::as_const( blockList ) ) {
            firstIndexesRows << block.firstIndex.row();
        }
        // END: order for marking as alternate those blocks that are alternate
        for (auto it = blocks.begin(); it != blocks.end(); ++it) {
            DesQUI::CategorizedViewPrivate::Block& block = *it;

            if ( block.firstIndex.row() > firstAffectedCategory.row() ) {
                block.outOfQuarantine = false;
                block.alternate       = firstIndexesRows.indexOf( block.firstIndex.row() ) % 2;
            }
            else if ( block.firstIndex.row() == firstAffectedCategory.row() ) {
                block.alternate = firstIndexesRows.indexOf( block.firstIndex.row() ) % 2;
            }
        }
    }
    // END: mark as in quarantine those categories that are under the affected ones
}


QRect DesQUI::CategorizedViewPrivate::mapToViewport( const QRect& rect ) const {
    // Q_D( const CategorizedView );

    const int dx = -q->horizontalOffset();
    const int dy = -q->verticalOffset();

    return rect.adjusted( dx, dy, dx, dy );
}


QRect DesQUI::CategorizedViewPrivate::mapFromViewport( const QRect& rect ) const {
    // Q_D( const CategorizedView );

    const int dx = q->horizontalOffset();
    const int dy = q->verticalOffset();

    return rect.adjusted( dx, dy, dx, dy );
}


int DesQUI::CategorizedViewPrivate::highestElementInLastRow( const Block& block ) const {
    // Q_D( const CategorizedView );

    // Find the highest element in the last row
    const QModelIndex lastIndex = proxyModel->index( block.firstIndex.row() + block.items.count() - 1, q->modelColumn(), q->rootIndex() );
    const QRect       prevRect  = q->visualRect( lastIndex );
    int               res       = prevRect.height();
    QModelIndex       prevIndex = proxyModel->index( lastIndex.row() - 1, q->modelColumn(), q->rootIndex() );

    if ( !prevIndex.isValid() ) {
        return res;
    }

    Q_FOREVER {
        const QRect tempRect = q->visualRect( prevIndex );

        if ( tempRect.topLeft().y() < prevRect.topLeft().y() ) {
            break;
        }

        res = qMax( res, tempRect.height() );

        if ( prevIndex == block.firstIndex ) {
            break;
        }

        prevIndex = proxyModel->index( prevIndex.row() - 1, q->modelColumn(), q->rootIndex() );
    }

    return res;
}


bool DesQUI::CategorizedViewPrivate::hasGrid() const {
    // Q_D( const CategorizedView );

    const QSize gridSize = q->gridSize();

    return gridSize.isValid() && !gridSize.isNull();
}


QString DesQUI::CategorizedViewPrivate::categoryForIndex( const QModelIndex& index ) const {
    const QModelIndex categoryIndex = index.model()->index( index.row(), proxyModel->sortColumn(), index.parent() );

    return categoryIndex.data( DesQUI::SortFilterProxyModel::CategoryDisplayRole ).toString();
}


void DesQUI::CategorizedViewPrivate::leftToRightVisualRect( const QModelIndex& index, Item& item, const Block& block, const QPoint& blockPos ) const {
    // Q_D( const CategorizedView );

    const int firstIndexRow = block.firstIndex.row();

    if ( hasGrid() ) {
        const int relativeRow    = index.row() - firstIndexRow;
        const int maxItemsPerRow = qMax( viewportWidth() / q->gridSize().width(), 1 );

        if ( q->layoutDirection() == Qt::LeftToRight ) {
            item.topLeft.rx() = (relativeRow % maxItemsPerRow) * q->gridSize().width() + blockPos.x() + categoryDrawer->leftMargin();
        }
        else {
            item.topLeft.rx() = viewportWidth() - ( (relativeRow % maxItemsPerRow) + 1) * q->gridSize().width() + categoryDrawer->leftMargin() + categorySpacing;
        }

        item.topLeft.ry() = (relativeRow / maxItemsPerRow) * q->gridSize().height();
    }
    else {
        if ( q->uniformItemSizes() ) {
            const int   relativeRow    = index.row() - firstIndexRow;
            const QSize itemSize       = q->sizeHintForIndex( index );
            const int   maxItemsPerRow = qMax( (viewportWidth() - q->spacing() ) / (itemSize.width() + q->spacing() ), 1 );

            if ( q->layoutDirection() == Qt::LeftToRight ) {
                item.topLeft.rx() = (relativeRow % maxItemsPerRow) * itemSize.width() + blockPos.x() + categoryDrawer->leftMargin();
            }
            else {
                item.topLeft.rx() = viewportWidth() - (relativeRow % maxItemsPerRow) * itemSize.width() + categoryDrawer->leftMargin() + categorySpacing;
            }

            item.topLeft.ry() = (relativeRow / maxItemsPerRow) * itemSize.height();
        }
        else {
            const QSize currSize = q->sizeHintForIndex( index );

            if ( index != block.firstIndex ) {
                const int   viewportW = viewportWidth() - q->spacing();
                QModelIndex prevIndex = proxyModel->index( index.row() - 1, q->modelColumn(), q->rootIndex() );
                QRect       prevRect  = q->visualRect( prevIndex );
                prevRect = mapFromViewport( prevRect );

                if ( (prevRect.bottomRight().x() + 1) + currSize.width() - blockPos.x() + q->spacing() > viewportW ) {
                    // we have to check the whole previous row, and see which one was the
                    // highest.
                    Q_FOREVER {
                        prevIndex = proxyModel->index( prevIndex.row() - 1, q->modelColumn(), q->rootIndex() );
                        const QRect tempRect = q->visualRect( prevIndex );

                        if ( tempRect.topLeft().y() < prevRect.topLeft().y() ) {
                            break;
                        }

                        if ( tempRect.bottomRight().y() > prevRect.bottomRight().y() ) {
                            prevRect = tempRect;
                        }

                        if ( prevIndex == block.firstIndex ) {
                            break;
                        }
                    }

                    if ( q->layoutDirection() == Qt::LeftToRight ) {
                        item.topLeft.rx() = categoryDrawer->leftMargin() + blockPos.x() + q->spacing();
                    }
                    else {
                        item.topLeft.rx() = viewportWidth() - currSize.width() + categoryDrawer->leftMargin() + categorySpacing;
                    }

                    item.topLeft.ry() = (prevRect.bottomRight().y() + 1) + q->spacing() - blockPos.y();
                }
                else {
                    if ( q->layoutDirection() == Qt::LeftToRight ) {
                        item.topLeft.rx() = (prevRect.bottomRight().x() + 1) + q->spacing();
                    }
                    else {
                        item.topLeft.rx() = (prevRect.bottomLeft().x() - 1) - q->spacing() - item.size.width() + categoryDrawer->leftMargin() + categorySpacing;
                    }

                    item.topLeft.ry() = prevRect.topLeft().y() - blockPos.y();
                }
            }
            else {
                if ( q->layoutDirection() == Qt::LeftToRight ) {
                    item.topLeft.rx() = blockPos.x() + categoryDrawer->leftMargin() + q->spacing();
                }
                else {
                    item.topLeft.rx() = viewportWidth() - currSize.width() + categoryDrawer->leftMargin() + categorySpacing;
                }

                item.topLeft.ry() = q->spacing();
            }
        }
    }

    item.size = q->sizeHintForIndex( index );
}


void DesQUI::CategorizedViewPrivate::topToBottomVisualRect( const QModelIndex& index, Item& item, const Block& block, const QPoint& blockPos ) const {
    // Q_D( const CategorizedView );

    const int firstIndexRow = block.firstIndex.row();

    if ( hasGrid() ) {
        const int relativeRow = index.row() - firstIndexRow;
        item.topLeft.rx() = blockPos.x() + categoryDrawer->leftMargin();
        item.topLeft.ry() = relativeRow * q->gridSize().height();
    }
    else {
        if ( q->uniformItemSizes() ) {
            const int   relativeRow = index.row() - firstIndexRow;
            const QSize itemSize    = q->sizeHintForIndex( index );
            item.topLeft.rx() = blockPos.x() + categoryDrawer->leftMargin();
            item.topLeft.ry() = relativeRow * itemSize.height();
        }
        else {
            if ( index != block.firstIndex ) {
                QModelIndex prevIndex = proxyModel->index( index.row() - 1, q->modelColumn(), q->rootIndex() );
                QRect       prevRect  = q->visualRect( prevIndex );
                prevRect          = mapFromViewport( prevRect );
                item.topLeft.rx() = blockPos.x() + categoryDrawer->leftMargin() + q->spacing();
                item.topLeft.ry() = (prevRect.bottomRight().y() + 1) + q->spacing() - blockPos.y();
            }
            else {
                item.topLeft.rx() = blockPos.x() + categoryDrawer->leftMargin() + q->spacing();
                item.topLeft.ry() = q->spacing();
            }
        }
    }

    item.size = q->sizeHintForIndex( index );
    item.size.setWidth( viewportWidth() );
}
