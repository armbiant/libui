/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This file was originally a part of the KDE project: KF5 ItemViews Module
 * 2019 David Redondo <kde@david-redondo.de>
 * 2007, 2009 Rafael Fernández López <ereslibre@kde.org>
 * Original License: LGPL 2.0
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "CategoryDrawer.hpp"

#include <QtCore/private/qobject_p.h>
#include <QApplication>
#include <QPainter>
#include <QStyleOption>

#include "SortFilterProxyModel.hpp"
#include "CategorizedView.hpp"

class DesQUI::CategoryDrawerPrivate : public QObjectPrivate {
    Q_DECLARE_PUBLIC( DesQUI::CategoryDrawer );

    public:
        CategoryDrawerPrivate( DesQUI::CategorizedView *view )
            : view( view ) {
        }

        ~CategoryDrawerPrivate() {
        }

        DesQUI::CategorizedView *const view;
};

DesQUI::CategoryDrawer::CategoryDrawer( DesQUI::CategorizedView *view ) : QObject( view ) {
}


DesQUI::CategoryDrawer::~CategoryDrawer() = default;

void DesQUI::CategoryDrawer::drawCategory( const QModelIndex& index, int /*sortRole*/, const QStyleOption& option, QPainter *painter ) const {
    // Keep this in sync with Kirigami.ListSectionHeader
    painter->setRenderHint( QPainter::Antialiasing );

    const QString category = index.model()->data( index, SortFilterProxyModel::CategoryDisplayRole ).toString();
    QFont         font( QApplication::font() );

    // Match Heading with level 3
    font.setPointSizeF( font.pointSize() * 1.20 );
    const QFontMetrics fontMetrics = QFontMetrics( font );

    QColor backgroundColor = option.palette.window().color();

    // BEGIN: background
    {
        QRect backgroundRect( option.rect );
        backgroundRect.setHeight( categoryHeight( index, option ) );
        painter->save();
        painter->setBrush( backgroundColor );
        painter->setPen( Qt::NoPen );
        painter->drawRect( backgroundRect );
        painter->restore();
    }
    // END: background

    // BEGIN: text
    {
        //  Kirgami.Units.{small/large}Spacing respectively
        constexpr int topPadding  = 4;
        constexpr int sidePadding = 8;
        QRect         textRect( option.rect );
        textRect.setTop( textRect.top() + topPadding );
        textRect.setLeft( textRect.left() + sidePadding );
        textRect.setHeight( fontMetrics.height() );
        textRect.setRight( textRect.right() - sidePadding );

        painter->save();
        painter->setFont( font );
        QColor penColor( option.palette.text().color() );
        painter->setPen( penColor );
        painter->drawText( textRect, Qt::AlignLeft | Qt::AlignVCenter, category );
        painter->restore();
    }
    // END: text
}


int DesQUI::CategoryDrawer::categoryHeight( const QModelIndex& index, const QStyleOption& option ) const {
    Q_UNUSED( index );
    Q_UNUSED( option )

    QFont font( QApplication::font() );

    font.setPointSizeF( font.pointSize() * 1.20 );
    QFontMetrics fontMetrics( font );

    const int height = fontMetrics.height() + 8;

    return height;
}


int DesQUI::CategoryDrawer::leftMargin() const {
    return 0;
}


int DesQUI::CategoryDrawer::rightMargin() const {
    return 0;
}


DesQUI::CategorizedView *DesQUI::CategoryDrawer::view() const {
    Q_D( const CategoryDrawer );

    return d->view;
}


void DesQUI::CategoryDrawer::mouseButtonPressed( const QModelIndex&, const QRect&, QMouseEvent *event ) {
    event->ignore();
}


void DesQUI::CategoryDrawer::mouseButtonReleased( const QModelIndex&, const QRect&, QMouseEvent *event ) {
    event->ignore();
}


void DesQUI::CategoryDrawer::mouseMoved( const QModelIndex&, const QRect&, QMouseEvent *event ) {
    event->ignore();
}


void DesQUI::CategoryDrawer::mouseButtonDoubleClicked( const QModelIndex&, const QRect&, QMouseEvent *event ) {
    event->ignore();
}


void DesQUI::CategoryDrawer::mouseLeft( const QModelIndex&, const QRect& ) {
}
