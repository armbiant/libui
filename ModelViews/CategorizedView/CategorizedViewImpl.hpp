/**
 * Copyright Marcus Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of LibDesQ Widgets (https://gitlab.com/DesQ/libdesqui)
 * This library contains the UI classes and Plugin interfaces that are used
 * through out the DesQ Project.
 *
 * This file was originally a part of the KDE project: KF5 ItemViews Module
 * 2007, 2009 Rafael Fernández López <ereslibre@kde.org>
 * Original License: LGPL 2.0
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "CategorizedView.hpp"
#include <QtCore/private/qobject_p.h>

namespace DesQUI {
    class SortFilterProxyModel;
    class CategoryDrawer;
    class CategorizedViewPrivate;
}

/**
 * @internal
 */
class DesQUI::CategorizedViewPrivate : public QObjectPrivate {
    DesQUI::CategorizedView *q;

    public:
        typedef struct _Item {
            QPoint topLeft;
            QSize  size;
        } Item;

        typedef struct _Block {
            bool operator!=( const _Block& rhs ) const {
                return firstIndex != rhs.firstIndex;
            }

            static bool lessThan( const _Block& left, const _Block& right ) {
                Q_ASSERT( left.firstIndex.isValid() );
                Q_ASSERT( right.firstIndex.isValid() );
                return left.firstIndex.row() < right.firstIndex.row();
            }

            QPoint                topLeft;
            int                   height = -1;
            QPersistentModelIndex firstIndex;
            // if we have n elements on this block, and we inserted an element at position i. The quarantine
            // will start at index (i, column, parent). This means that for all elements j where i <= j <=
            // n, the
            // visual rect position of item j will have to be recomputed (cannot use the cached point). The
            // quarantine
            // will only affect the current block, since the rest of blocks can be affected only in the way
            // that the whole block will have different offset, but items will keep the same relative
            // position
            // in terms of their parent blocks.
            QPersistentModelIndex quarantineStart;
            QList<Item>           items;

            // this affects the whole block, not items separately. items contain the topLeft point relative
            // to the block. Because of insertions or removals a whole block can be moved, so the whole
            // block
            // will enter in quarantine, what is faster than moving all items in absolute terms.
            bool                  outOfQuarantine = false;

            // should we alternate its color ? is just a hint, could not be used
            bool                  alternate = false;
            bool                  collapsed = false;
        } Block;

        explicit CategorizedViewPrivate( DesQUI::CategorizedView *qq );
        ~CategorizedViewPrivate();

        /**
         * @return whether this view has all required elements to be categorized.
         */
        bool isCategorized() const;

        /**
         * @return the block rect for the representative @p representative.
         */
        QStyleOptionViewItem blockRect( const QModelIndex& representative );

        /**
         * Returns the first and last element that intersects with rect.
         *
         * @note see that here we cannot take out items between first and last (as we could
         *       do with the rubberband).
         *
         * Complexity: O(log(n)) where n is model()->rowCount().
         */
        QPair<QModelIndex, QModelIndex> intersectingIndexesWithRect( const QRect& rect ) const;

        /**
         * Returns the position of the block of @p category.
         *
         * Complexity: O(n) where n is the number of different categories when the block has been
         *             marked as in quarantine. O(1) the rest of the times (the vast majority).
         */
        QPoint blockPosition( const QString& category );

        /**
         * Returns the height of the block determined by @p category.
         */
        int blockHeight( const QString& category );

        /**
         * Returns the actual viewport width.
         */
        int viewportWidth() const;

        /**
         * Marks all elements as in quarantine.
         *
         * Complexity: O(n) where n is model()->rowCount().
         *
         * @warning this is an expensive operation
         */
        void regenerateAllElements();

        /**
         * Update internal information, and keep sync with the real information that the model contains.
         */
        void rowsInserted( const QModelIndex& parent, int start, int end );

        /**
         * Returns @p rect in viewport terms, taking in count horizontal and vertical offsets.
         */
        QRect mapToViewport( const QRect& rect ) const;

        /**
         * Returns @p rect in absolute terms, converted from viewport position.
         */
        QRect mapFromViewport( const QRect& rect ) const;

        /**
         * Returns the height of the highest element in last row. This is only applicable if there is
         * no grid set and uniformItemSizes is false.
         *
         * @param block in which block are we searching. Necessary to stop the search if we hit the
         *              first item in this block.
         */
        int highestElementInLastRow( const Block& block ) const;

        /**
         * Returns whether the view has a valid grid size.
         */
        bool hasGrid() const;

        /**
         * Returns the category for the given index.
         */
        QString categoryForIndex( const QModelIndex& index ) const;

        /**
         * Updates the visual rect for item when flow is LeftToRight.
         */
        void leftToRightVisualRect( const QModelIndex& index, Item& item, const Block& block, const QPoint& blockPos ) const;

        /**
         * Updates the visual rect for item when flow is TopToBottom.
         * @note we only support viewMode == ListMode in this case.
         */
        void topToBottomVisualRect( const QModelIndex& index, Item& item, const Block& block, const QPoint& blockPos ) const;

        /**
         * Called when expand or collapse has been clicked on the category drawer.
         */
        // void _k_slotCollapseOrExpandClicked( QModelIndex );

        DesQUI::SortFilterProxyModel *proxyModel = nullptr;
        DesQUI::CategoryDrawer *categoryDrawer   = nullptr;
        int categorySpacing         = 0;
        bool alternatingBlockColors = false;
        bool collapsibleBlocks      = false;

        Block *hoveredBlock;
        QString hoveredCategory;
        QModelIndex hoveredIndex;

        QPoint pressedPosition;
        QRect rubberBandRect;

        QHash<QString, Block> blocks;
};
